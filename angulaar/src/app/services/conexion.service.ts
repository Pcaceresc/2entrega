import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

import { Docente } from '../models/docente';

@Injectable({
  providedIn: 'root'
})
export class ConexionService {

  docenteList: AngularFireList<any>;
  seleccion: Docente = new Docente();

  constructor(private firebase: AngularFireDatabase){

    
  }

  getDocentes(){
      return this.docenteList = this.firebase.list('docentes');
  }

  setDocente(docente: Docente){
    this.docenteList.push({
      name: docente.name,
      descripcion: docente.descripcion,
      edad: docente.edad,
      ramos: docente.ramos,
      ventajas: docente.ventajas,
      desventajas: docente.desventajas
    });
  }

  updateDocente(docente:Docente){
    this.docenteList.update(docente.name, {
      name: docente.name,
      descripcion: docente.descripcion,
      edad: docente.edad,
      ramos: docente.ramos,
      ventajas: docente.ventajas,
      desventajas: docente.desventajas
    });
  }

  deleteDocente($key: string){
    this.docenteList.remove($key); 
  }
}
