import { Component, OnInit } from '@angular/core';
import { ConexionService } from '../services/conexion.service';
import { NgForm } from '@angular/forms';
import { Docente } from '../models/docente';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-docente',
  templateUrl: './docente.component.html',
  styleUrls: ['./docente.component.scss']
})
export class DocenteComponent implements OnInit {

   
  constructor(private conexion: ConexionService,
    private toastr: ToastrService){
    
  }

  docenteLista : Docente[];

  ngOnInit() {
    
    this.conexion.getDocentes()
      .snapshotChanges().subscribe(item => {
        this.docenteLista = [];
        item.forEach(element => {
          let x = element.payload.toJSON();
          x["$key"] = element.key;
          this.docenteLista.push(x as Docente);
        });
      });    
  }

  onSubmit(DocenteForm: NgForm){
    if(DocenteForm.value.$key == null){
      this.conexion.setDocente(DocenteForm.value);
    }else{
    this.conexion.updateDocente(DocenteForm.value);
    this.resetForm(DocenteForm);
    }
  }
  resetForm(DocenteForm? : NgForm){
    if (DocenteForm != null){
      DocenteForm.reset();
      this.conexion.seleccion = new Docente();
    }

  }

  onEdit(docente: Docente) {
    this.conexion.seleccion = Object.assign({}, docente);
  }

  onDelete($key: string) {
    if(confirm('Estas seguro que quieres eliminar este Docente?')) {
      this.conexion.deleteDocente($key);
    }

}}
